<?php
use Drupal\Core\Config\FileStorage;
use Drush\Log\LogLevel;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageComparer;

/**
 * @file
 * Drush integration for the config_processor module.
 */

/**
 * Implements hook_drush_command().
 */
function config_processor_drush_command() {
  $items['config-processor'] = array(
    'description' => 'Create a full configuration from one or more targets.',
    'core' => array('8+'),
    'aliases' => array('cproc'),
    'arguments' => array(
      'targets' => 'Configuration targets, comma separated and in order (keys in $config_directories array in settings.php).',
      'destination' => 'A config destination label (a key in $config_directories array in settings.php).',
    ),
    'options' => array(
      'changelist' => 'Shows the list of changed active config.',
      'synchronized' => 'Shows the list of config that matches active config.',
      'show-destinations' => array(
        'description' => 'Choose from a list of config destinations',
      ),
    ),
    'required-arguments' => TRUE,
    'examples' => array(
      'drush cproc sync,debug development' => 'Creates a development directory from the sync and dev targets.',
      'drush cproc sync,coding,debug local' => 'Creates a local directory from the sync, coding, and dev targets.',
      'drush cproc sync,security,optimized production' => 'Creates a production directory from the sync, security, and optimized targets.',
    ),
  );
  return $items;
}

/**
 * Drush command callback to build config targets.
 *
 * @param string $targets
 *   Comma separated list of specific configuration directories in order.
 * @param string $destination
 *   Config directory for the target.
 *
 * @return bool
 *   Success of writing the partial config to the destination.
 */
function drush_config_processor($targets, $destination) {
  global $config_directories;
  $choices = drush_map_assoc(array_keys($config_directories));
  $changelist = drush_get_option('changelist', 0);
  $synchronized = drush_get_option('synchronized', 0);

  // Check if destination doesn't exist.
  if (!in_array($destination, $choices)) {
    $msg = dt('Error !target not found as a configuration target.',
      ['!target' => $destination]);
    return drush_set_error('NO_CONFIG_DEST', $msg);
  }

  // Check each target specified.
  $target_directories = explode(',', $targets);
  foreach ($target_directories as $target_dir) {
    if (!in_array($target_dir, $choices)) {
      $msg = dt('Error !target not found as a configuration target.',
        ['!target' => $target_dir]);
      return drush_set_error('NO_CONFIG_TARGET', $msg);
    }
  }

  // Build full set of config.
  $combined_config = [];
  $target_storages = [];
  foreach ($target_directories as $target) {
    $target_dir = config_get_config_directory($target);
    $target_storage = new FileStorage($target_dir);
    $target_keys = $target_storage->listAll();
    foreach ($target_keys as $target_key) {
      $combined_config[$target_key] = $target;
    }
    $target_storages[$target] = $target_storage;
  }

  // Create changelog from active config.
  $changelog = [];
  /** @var \Drupal\Core\Config\CachedStorage $active_storage */
  $active_storage = \Drupal::service('config.storage');
  $active_config_list = $active_storage->listAll();
  foreach ($active_config_list as $active_config_item) {
    // Check to see if the config has been defined.
    if (!empty($combined_config[$active_config_item])) {
      $overlapping_target = $combined_config[$active_config_item];
      // Check if the config varies from active config.
      $active_raw_data = $active_storage->read($active_config_item);
      $target_raw_data = $target_storages[$overlapping_target]->read($active_config_item);
      if ($active_raw_data != $target_raw_data) {
        $changelog[$active_config_item] = [
          'operation' => 'update',
          'target' => $overlapping_target,
        ];
      }
    }
    else {
      // No definition exists in the proposed config.
      $changelog[$active_config_item] = [
        'operation' => 'delete',
        'target' => '',
      ];
    }
  }

  // Finally check keys that are in combined config but not in active.
  $added_keys = array_diff(array_keys($combined_config), array_values($active_config_list));
  foreach ($added_keys as $added_key => $added_value) {
    $changelog[$added_value] = [
      'operation' => 'insert',
      'target' => $combined_config[$added_value],
    ];
  }

  // Print the changelog and confirm.
  if ($changelist) {
    ksort($changelog);
    drush_print(dt("Changes from your active configuration:"));
    $rows = [];
    $rows[] = ['Config', 'Operation', 'Target'];
    foreach ($changelog as $changed_item => $changed_values) {
      $rows[] = [
        $changed_item,
        $changed_values['operation'],
        $changed_values['target'],
      ];
    }

    drush_print_table($rows, TRUE);
  }

  if ($synchronized) {
    $changelog = [];
    $synchronized_list = array_intersect(array_values($active_config_list), array_keys($combined_config));
    // Let's show the rest as in sync.
    foreach ($synchronized_list as $sync_key) {
      $changelog[$sync_key] = 'sync';
    }
    ksort($changelog);
    asort($changelog);
    drush_print(dt("Synchronized with your active configuration:"));
    foreach ($changelog as $changed_item => $changed_operation) {
      drush_print(dt($changed_item . ' from the target ' . $combined_config[$changed_item]));
    }
  }

  if (drush_confirm(dt('Do you wish to write this configuration to the ' . $destination . ' target?'))) {
    $destination_dir = config_get_config_directory($destination);
    $destination_storage = new FileStorage($destination_dir);
    foreach ($combined_config as $combined_item => $combined_target) {
      $target_raw_data = $target_storages[$combined_target]->read($combined_item);
      $destination_storage->write($combined_item, $target_raw_data);
      drush_log(dt('Writing !name to !target.', array(
        '!name' => $combined_item,
        '!target' => $destination_dir,
      )), LogLevel::SUCCESS);
    }
  }
  return TRUE;
}
